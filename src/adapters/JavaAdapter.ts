import {Adapter} from '../Adapter';

export class JavaAdapter extends Adapter {
    constructor() {
        super();
        this.id = 'JavaAdapter';
    }
    update(evt: Object): boolean {
        if (this.isActive()) {
            (window as any)['JavaBridge'].executeChange(evt);
            return true;
        }
        return false;
    }

    public isActive(): boolean {
        return (window as any)['JavaBridge'];
    }
}
