import {VirtualKeyBoard} from './VirtualKeyBoard';

export {Point} from './elements/BaseElements';
export {Bridge, DelegateAdapter} from './Bridge';
export {Graph} from './elements/Graph';
export * from './elements/nodes';
export * from './elements/edges';
export * from './adapters';
export * from './UML';
import {Point} from './elements/BaseElements';
import {Graph} from './elements/Graph';
import {ClassEditor} from './elements/ClassEditor';
import {Bridge} from './Bridge';
import {Util} from './util';
import * as nodes  from './elements/nodes';
import * as edges from './elements/edges';
import {ScrumBoard} from './ScrumBoard';

if (!(window as any)['Point']) {
    (window as any)['Point'] = Point;
    (window as any)['Graph'] = Graph;
    (window as any)['bridge'] = new Bridge();
    (window as any)['Util'] = Util;
    (window as any)['Class'] = nodes.Class;
    (window as any)['Association'] = edges.Association;
    (window as any)['SymbolLibary'] = nodes.SymbolLibary;
    (window as any)['ClassEditor'] = ClassEditor;
    (window as any)['VirtualKeyBoard'] = VirtualKeyBoard;
    (window as any)['ScrumBoard'] = ScrumBoard;
}
